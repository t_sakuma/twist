module.exports = {
  entry: __dirname + "/coffee/Main",
  output: {
    path: __dirname + '/',
    filename: 'app.js'
  },

  module: {
    loaders: [
      {
        test: /\.coffee$/,
        exclude: /node_modules/,
        loader: 'coffee-loader'
      }
    ]
  },
  devtool: 'source-map',
  resolve: {
    extensions: ['', '.js', '.coffee'],
    modulesDirectories: ['coffee']
  },
  plugins: [
    function () {
      this.plugin('watch-run', (watching, callback) => {
        console.log('\033[36m' + 'Begin compile at ' + new Date() + ' \033[39m')
        callback()
      })
    }
  ]
};
