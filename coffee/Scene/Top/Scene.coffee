Scene  = require 'Core/Base/Scene'
UraScene  = require 'Scene/Stage/Stage5/Scene'
Se  = require 'Core/Base/Se'
Bg  = require './Bg'
Config = require 'Config'
Collision = require 'Core/Base/Collision'

module.exports = enchant.Class.create Scene,
  initialize: () ->
    Scene.call @, 'assets/op.mp3'
    Title = require './Title'
    StartLabel = require './StartLabel'
    UraLabel = require './UraLabel'
    Start = require './Start'
    Player = require './Player'
    Floor = require './Floor'
    @bg.addChild new Bg()
    @bg.addChild new Title()
    @hud2.addChild new StartLabel()
    @hud2.addChild new Start()
    if enchant.Core.instance.clearFlg
      @hud2.addChild new UraLabel()
    @player = new Player()
    @bg.addChild @player
    _.each [0..11] , (x) =>
      @bg.addChild new Floor(x * 128, Config.windowSize.height - 128)
    @NextScene = require 'Scene/Stage/Stage1/Scene'
    @wait = false

    @on 'enterframe', () ->
      core = enchant.Core.instance
      if !enchant.Core.instance.debug2 && core.input.up && core.input.down && core.input.left && core.input.right
        Se.play 'mou', 'wav'
        enchant.Core.instance.debug2 = true

      if (@wait)
        return

      nodes = Collision.eachHitCheck @cur, @hud2.childNodes
      if Collision.existTypeOfNode nodes, 'start'
        @player.status = 'run'
        @moveNextScene()

      nodes = Collision.eachHitCheck @cur, @hud2.childNodes
      if Collision.existTypeOfNode nodes, 'ura'
        @player.status = 'run'
        @moveUraScene()

  moveNextScene: ->
    @wait = true
    @tl
    .then ->
      Se.play 'button2'
    .delay 60
    .then =>
      @replaceScene new @NextScene()

  moveUraScene: ->
    @wait = true
    @tl
    .then ->
      Se.play 'button2'
    .delay 60
    .then =>
      @replaceScene new UraScene()
