Config = require 'Config'
LabelObj = require 'Core/Base/LabelObj'

module.exports = enchant.Class.create LabelObj,
  initialize: () ->
    LabelObj.call @
    @type = 'ura'
    @text = '隠しステージ'
    @width = 300
    @height = 70
    @x = (Config.windowSize.width - @width)
    @y = (Config.windowSize.height - @height - (128 * 4))
    @font = '40px \'Arial\''
    @textAlign = 'center'

  onenterframe: (e) ->
    core = enchant.Core.instance
    @opacity = 0.5 + Math.abs(Math.sin(core.frame / 10)) / 2
