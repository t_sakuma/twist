Config = require 'Config'

module.exports = enchant.Class.create enchant.Sprite,
  initialize: () ->
    enchant.Sprite.call @
    core = enchant.Core.instance
    @image = core.assets['assets/char.png']
    @frame = 0
    @width = 128
    @height = 128
    @x = (@width)
    @y = (Config.windowSize.height - @height - 128)

  onenterframe: (e) ->
    core = enchant.Core.instance
    @x = @move @status, @x
    @frame = @motion @status, core.frame

  move: (status, x) ->
    if status == 'run'
      x + 30
    else
      x

  motion: (status, count) ->
    if status == 'run'
      Math.floor(count / 3) % 4 + 5
    else
      Math.floor(count / 7) % 1
