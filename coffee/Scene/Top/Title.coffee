Config = require 'Config'

module.exports = enchant.Class.create enchant.Sprite,
  initialize: () ->
    enchant.Sprite.call @
    core = enchant.Core.instance

    @scaleX = 1.2
    @scaleY = 1.2
    @width = 1004
    @height = 236
    @x = (Config.windowSize.width - @width) / 2
    @y = 100
    @image = core.assets['assets/title.png']
