Config = require 'Config'
LabelObj = require 'Core/Base/LabelObj'

module.exports = enchant.Class.create LabelObj,
  initialize: () ->
    LabelObj.call @
    @type = 'start'
    @text = 'Touch!<br /><br /><br />　　↓'
    @width = 250
    @height = 100
    @x = (Config.windowSize.width - @width)
    @y = (Config.windowSize.height - @height - (128 * 2))
    @font = 'bold 50px \'Arial\''
    @textAlign = 'center'

  onenterframe: (e) ->
    core = enchant.Core.instance
    @opacity = 0.5 + Math.abs(Math.sin(core.frame / 20)) / 2
