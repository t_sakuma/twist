Config = require 'Config'

module.exports = enchant.Class.create enchant.Sprite,
  initialize: () ->
    enchant.Sprite.call @
    @type = 'start'
    @image = enchant.Core.instance.assets['assets/item.png']
    @frame = 0
    @width = 128
    @height = 128
    @x = (Config.windowSize.width - @width)
    @y = (Config.windowSize.height - @height - 128)

  getBound: ->
    left : @x
    top : @y
    right: @x + @width
    bottom: @y + @height
