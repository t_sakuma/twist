Config = require 'Config'

module.exports = enchant.Class.create enchant.Label,
  initialize: (text, fontSize = 30) ->
    enchant.Label.call @
    @text = text
    @width = Config.windowSize.width
    @y = Config.windowSize.height
    @font = "#{fontSize}px 'Arial'"
    @textAlign = 'center'

  onenterframe: () ->
    @y--
