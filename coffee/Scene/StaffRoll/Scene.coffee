Scene         = require 'Core/Base/Scene'
Config        = require 'Config'

module.exports = enchant.Class.create Scene,
  initialize: () ->
    Scene.call @
    @ScrollLabel = require './ScrollLabel'
    @BgSprite = require './BgSprite'
    @NextScene = require 'Scene/Top/Scene'

    core = enchant.Core.instance
    core.assets['assets/ed.mp3'].clone().play()

    @tl
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('魔法少女製作委員会')
        _.each [0..6], (y)=>
          _.each [0..11], (x)=>
            @bg.addChild new @BgSprite(x, y, 'assets/map4.png', 1, 30 * 150)
        @bg.addChild new @BgSprite(0, 6, 'assets/map4.png')
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('藪崎　真紀')
        @bg.addChild new @BgSprite(1, 6, 'assets/map4.png')
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('佐久間 貴弘')
        @bg.addChild new @BgSprite(2, 6, 'assets/map4.png')
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('前山 周')
        @bg.addChild new @BgSprite(3, 6, 'assets/map4.png')
      .delay 30
      .delay 30
      .delay 30
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('企画')
        @bg.addChild new @BgSprite(4, 6, 'assets/map4.png')
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('佐久間 貴弘')
        @bg.addChild new @BgSprite(5, 6, 'assets/map4.png')
        @bg.addChild new @BgSprite(1, 5, 'assets/object.png', 1)
        @bg.addChild new @BgSprite(1, 4, 'assets/object.png', 1)
        @bg.addChild new @BgSprite(2, 3, 'assets/object.png', 0)
        @bg.addChild new @BgSprite(3, 3, 'assets/object.png', 0)
      .delay 30
      .delay 30
      .delay 30
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('システム')
        @bg.addChild new @BgSprite(6, 6, 'assets/map4.png')
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('佐久間 貴弘')
        @bg.addChild new @BgSprite(7, 6, 'assets/map4.png')
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('前山　周')
        @bg.addChild new @BgSprite(8, 6, 'assets/object.png', 5)
      .delay 30
      .delay 30
      .delay 30
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('グラフィック')
        @bg.addChild new @BgSprite(9, 6, 'assets/map4.png')
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('藪崎 真紀')
        @bg.addChild new @BgSprite(10, 6, 'assets/map4.png')
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('佐久間　貴弘')
        @bg.addChild new @BgSprite(9, 5, 'assets/map4.png')
      .delay 30
      .delay 30
      .delay 30
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('二日酔い')
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('前山 周')
      .delay 30
      .delay 30
      .delay 30
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('ハイパーメディアクリエイター')
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('藪崎 真紀')
      .delay 30
      .delay 30
      .delay 30
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('超監督')
        @bg.addChild new @BgSprite(11, 6, 'assets/map4.png')
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('佐久間　貴弘', 60)
      .delay 30
      .delay 30
      .delay 30
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('お借りした素材')
        @bg.addChild new @BgSprite(8, 4, 'assets/char.png', 10)
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('無料効果音で遊ぼう！様')
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('女の子の声などのSE')
      .delay 30
      .delay 30
      .delay 30
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('ニコニ・コモンズよりお借りした素材')
        @bg.addChild new @BgSprite(9, 4, 'assets/boss.png')
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('【BGM集】女の子- Instrumental - 【かわいい系】　McCoy様')
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('【BGM集】女の子 - 初夏 version - 【かわいい系】　McCoy様')
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('【BGM集】潜入【シリアス系】　McCoy様')
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('【BGM集】無人ファクトリー【シリアス系】　McCoy様')
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('【BGM集】いつもと違う街【シリアス系】　McCoy様')
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('［効果音］ドア開ける　On-Jin ～音人～様')
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('Bad Apple!!　弦楽合奏版【改】　LK704様')
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('【東方夢時空】ピアノでゲームオーバー　カニさし様')
      .delay 30
      .delay 30
      .delay 30
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('スペシャルサンクス')
      .delay 30
      .delay 30
      .delay 30
      .delay 30
      .then ->
        @hud2.addChild new @ScrollLabel('And You!')
      .delay 30 * 30
      .then ->
        enchant.Core.instance.clearFlg = true
        @replaceScene new @NextScene()
