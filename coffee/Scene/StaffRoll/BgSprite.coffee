Config = require 'Config'

module.exports = enchant.Class.create enchant.Sprite,
  initialize: (x, y, asset, frame = 0, time = 150) ->
    enchant.Sprite.call @
    core = enchant.Core.instance
    @image = core.assets[asset]
    @frame = frame
    @width = 128
    @height = 128
    @x = 128 * x
    @y = 128 * y
    @opacity = 0

    @tl.fadeTo 0.5, time
