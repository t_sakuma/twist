Config = require 'Config'

module.exports = enchant.Class.create enchant.Sprite,
  initialize: (x, y) ->
    enchant.Sprite.call @
    core = enchant.Core.instance
    @image = core.assets['assets/map1.png']
    @frame = 0
    @width = 128
    @height = 128
    @x = x
    @y = y
