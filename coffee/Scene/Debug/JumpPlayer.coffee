Config = require 'Config'

module.exports = enchant.Class.create enchant.Sprite,
  initialize: () ->
    enchant.Sprite.call @
    core = enchant.Core.instance
    @image = core.assets['assets/char.png']
    @frame = 11
    @width = 128
    @height = 128
    @x = (@width * 3)
    @y = (Config.windowSize.height / 2)

  onenterframe: (e) ->
    core = enchant.Core.instance
    @y = @move @y
    @frame = @motion()

  move: (y) ->
    if Config.windowSize.height < y
      0
    else
      y + 20

  motion: ->
    10
