Config = require 'Config'

module.exports = enchant.Class.create enchant.Label,
  initialize: () ->
    enchant.Label.call @
    @text = ''
    @width = Config.windowSize.width
    @height = Config.windowSize.height
    @x = 0
    @y = 0
    @backgroundColor = '#8a6e0c'
