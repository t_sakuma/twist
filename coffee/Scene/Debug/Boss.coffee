Config = require 'Config'

module.exports = enchant.Class.create enchant.Sprite,
  initialize: () ->
    enchant.Sprite.call @
    core = enchant.Core.instance
    @image = core.assets['assets/boss.png']
    @frame = 0
    @width = 128
    @height = 128
    @x = (Config.windowSize.width - @width)
    @y = (Config.windowSize.height - 128 - @height)
