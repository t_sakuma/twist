Scene  = require 'Core/Base/Scene'
Config = require 'Config'

module.exports = enchant.Class.create Scene,
  initialize: () ->
    Scene.call @, 'assets/op.mp3'
    Player = require './Player'
    RunPlayer = require './RunPlayer'
    JumpPlayer = require './JumpPlayer'
    Gapao = require './Gapao'
    Boss = require './Boss'
    Floor = require './Floor'
    Bg = require './Bg'

    @bg.addChild new Bg()

    _.each [0..11] , (x) =>
      @bg.addChild new Floor(x * 128, Config.windowSize.height - 128)

    @hud2.addChild new Player()
    @hud2.addChild new RunPlayer()
    @hud2.addChild new JumpPlayer()
    @hud2.addChild new Gapao()
    @hud2.addChild new Boss()
