Config = require 'Config'

module.exports = enchant.Class.create enchant.Sprite,
  initialize: () ->
    enchant.Sprite.call @
    core = enchant.Core.instance
    @image = core.assets['assets/char.png']
    @frame = 0
    @width = 128
    @height = 128
    @x = (@width)
    @y = (Config.windowSize.height - @height - 128)

  onenterframe: (e) ->
    core = enchant.Core.instance
    @frame = @motion core.frame

  motion: (count) ->
    Math.floor(count / 7) % 1
