Config = require 'Config'

module.exports = enchant.Class.create enchant.Sprite,
  initialize: () ->
    enchant.Sprite.call @
    core = enchant.Core.instance
    @image = core.assets['assets/char.png']
    @frame = 0
    @width = 128
    @height = 128
    @x = 0
    @y = (Config.windowSize.height - @height - 128)

  onenterframe: (e) ->
    core = enchant.Core.instance
    @x = @move @x
    @frame = @motion core.frame

  move: (x) ->
    if (@width * 10) < x
      0
    else
      x + 10

  motion: (count) ->
    Math.floor(count / 3) % 4 + 5
