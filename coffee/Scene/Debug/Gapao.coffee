Config = require 'Config'

module.exports = enchant.Class.create enchant.Sprite,
  initialize: () ->
    enchant.Sprite.call @
    core = enchant.Core.instance
    @image = core.assets['assets/cur.png']
    @frame = 0
    @width = 64
    @height = 64
    @x = (@width)
    @y = (Config.windowSize.height / 2)
    @opacity = 0.5
