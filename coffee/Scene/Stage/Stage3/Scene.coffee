Config  = require 'Config'
Scene   = require 'Scene/Stage/Base/Stage'
NextScene  = require 'Scene/Stage/Stage4/Scene'
Goal    = require 'Scene/Stage/Base/Goal'
Thorn   = require 'Scene/Stage/Base/Thorn'
Jump   = require 'Scene/Stage/Base/Jump'
BoxPoint = require 'Scene/Stage/Base/BoxPoint'
FlBoxPoint = require 'Scene/Stage/Base/FlBoxPoint'
FallBoxPoint = require 'Scene/Stage/Base/FallBoxPoint'
TwistPoint = require 'Scene/Stage/Base/TwistPoint'
Player  = require 'Scene/Stage/Base/Player'
Bg      = require './Bg'
Dungeon = require './Dungeon'

module.exports = enchant.Class.create Scene,
  initialize: () ->
    Scene.call @, 'assets/stage3.mp3', 'フリーズマウンテン', require 'Scene/Stage/Stage3/Scene'
    @Scene = require 'Scene/Stage/Stage3/Scene'

    @bg.addChild new Bg()

    @dungeon = new Dungeon()
    @dg.addChild @dungeon

    @addObj new Goal(8, 9, NextScene)

    @player = new Player(1, 18)
    @dg.addChild @player

    @addPointer new BoxPoint(2, 6)
    @addPointer new BoxPoint(3, 6)
    @addPointer new BoxPoint(4, 6)
    @addPointer new BoxPoint(5, 6)
    @addPointer new BoxPoint(6, 5)
    @addPointer new BoxPoint(7, 6)
    @addPointer new BoxPoint(8, 6)
    @addPointer new BoxPoint(7, 15)
    @addPointer new BoxPoint(9, 12)
    @addPointer new TwistPoint(0, 3)
    @addPointer new TwistPoint(0, 10)
    @addPointer new TwistPoint(9, 5)
    @addPointer new TwistPoint(11, 16)
    @addPointer new FallBoxPoint(3, 5)
    @addPointer new FallBoxPoint(4, 4)
    @addPointer new FallBoxPoint(5, 5)
    @addPointer new FallBoxPoint(6, 6)
    @addPointer new FallBoxPoint(7, 5)
    @addPointer new FallBoxPoint(8, 4)
    @addPointer new FallBoxPoint(7, 16)
    @addPointer new FallBoxPoint(7, 17)
    @addPointer new FlBoxPoint(10, 17, 10)
    @addObj new Jump(2, 3)
    @addObj new Thorn(3, 3)
    @addObj new Thorn(4, 3)
    @addObj new Thorn(5, 3)
    @addObj new Thorn(6, 3)
    @addObj new Thorn(7, 3)
    @addObj new Thorn(8, 3)
