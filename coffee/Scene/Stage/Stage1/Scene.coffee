Config  = require 'Config'
Scene   = require 'Scene/Stage/Base/Stage'
NextScene  = require 'Scene/Stage/Stage2/Scene'
Goal    = require 'Scene/Stage/Base/Goal'
Thorn   = require 'Scene/Stage/Base/Thorn'
Jump   = require 'Scene/Stage/Base/Jump'
BoxPoint = require 'Scene/Stage/Base/BoxPoint'
FlBoxPoint = require 'Scene/Stage/Base/FlBoxPoint'
FallBoxPoint = require 'Scene/Stage/Base/FallBoxPoint'
TwistPoint = require 'Scene/Stage/Base/TwistPoint'
Player  = require 'Scene/Stage/Base/Player'
Bg      = require './Bg'
Dungeon = require './Dungeon'

module.exports = enchant.Class.create Scene,
  initialize: () ->
    Scene.call @, 'assets/stage1.mp3', 'チュートリアルコース', require 'Scene/Stage/Stage1/Scene'
    @Scene = require 'Scene/Stage/Stage1/Scene'

    @bg.addChild new Bg()

    @dungeon = new Dungeon()
    @dg.addChild @dungeon

    @addObj new Goal(1, 17, NextScene)

    @player = new Player(1, 19)
    @dg.addChild @player

    @addPointer new BoxPoint(4, 20)
    @addPointer new TwistPoint(8, 19)
    @addPointer new TwistPoint(17, 6)
    @addPointer new TwistPoint(1, 8)
    @addPointer new FallBoxPoint(10, 14)
    @addPointer new FallBoxPoint(11, 14)
    @addPointer new FlBoxPoint(11, 7, 5)
    @addObj new Jump(3, 6)
    @addObj new Thorn(8, 15)
    @addObj new Thorn(11, 10)
    @addObj new Thorn(7, 5)
    @addObj new Thorn(8, 5)
    @addObj new Thorn(9, 5)
    @addObj new Thorn(10, 5)
