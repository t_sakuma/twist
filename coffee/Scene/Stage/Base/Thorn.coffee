Config = require 'Config'
Obj = require './Obj'

module.exports = enchant.Class.create Obj,
  initialize: (x, y) ->
    Obj.call @, x, y
    core = enchant.Core.instance

    @image = core.assets['assets/object.png']
    @frame = 2
    @type = 'Thorn'
    @rect =
      x: 16
      y: 16
      w: 96
      h: 96

  onenterframe: ->
    if @wait
      return

    @motion()

  fall: (gravity) ->

  motion: () ->
    @count++
    @tl.rotateBy 3, 0
    if (@count > 100000)
      @count = 0

  setLanding: (flg) ->
