Config = require 'Config'
Pointer = require './Pointer'
Box = require './Box'

module.exports = enchant.Class.create Pointer,
  initialize: (x, y) ->
    Pointer.call @, x, y
    core = enchant.Core.instance

    @image = core.assets['assets/object.png']
    @frame = 0
    @opacity = 0.5
    @rect =
      x: 0
      y: 0
      w: 128
      h: 128
    @setProc () =>
      currentScene = enchant.Core.instance.currentScene
      currentScene.addObj new Box Math.round(@x / 128), Math.round(@y / 128)
