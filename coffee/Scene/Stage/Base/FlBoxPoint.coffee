Config = require 'Config'
Pointer = require './Pointer'
FlBox = require './FlBox'

module.exports = enchant.Class.create Pointer,
  initialize: (x, y, move) ->
    Pointer.call @, x, y
    core = enchant.Core.instance

    @image = core.assets['assets/object.png']
    @frame = 4
    @move = move
    @opacity = 0.5
    @rect =
      x: 0
      y: 0
      w: 128
      h: 64
    @setProc () =>
      currentScene = enchant.Core.instance.currentScene
      currentScene.addObj new FlBox Math.round(@x / 128), Math.round(@y / 128), @move
