Config = require 'Config'
Pointer = require './Pointer'
FallBox = require './FallBox'

module.exports = enchant.Class.create Pointer,
  initialize: (x, y) ->
    Pointer.call @, x, y
    core = enchant.Core.instance

    @image = core.assets['assets/object.png']
    @frame = 1
    @opacity = 0.5
    @rect =
      x: 0
      y: 0
      w: 128
      h: 128
    @setProc () =>
      currentScene = enchant.Core.instance.currentScene
      currentScene.addObj new FallBox Math.round(@x / 128), Math.round(@y / 128)
