Config = require 'Config'
LabelObj = require 'Core/Base/LabelObj'

module.exports = enchant.Class.create LabelObj,
  initialize: (x, y) ->
    LabelObj.call @
    @text = 'やりなおす'
    @type = 'restart'
    @color = '#FFF'
    @x = x
    @y = y
    @font = '40px \'Arial\''
    @textAlign = 'center'
