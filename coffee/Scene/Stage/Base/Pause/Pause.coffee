Config = require 'Config'
Bg = require './Bg'
ContinueLabel = require './ContinueLabel'
EndLabel = require './EndLabel'
RestartLabel = require './RestartLabel'
Collision = require 'Core/Base/Collision'
Se  = require 'Core/Base/Se'

module.exports = enchant.Class.create enchant.Group,
  initialize: () ->
    enchant.Group.call @
    core = enchant.Core.instance
    @TopScene = require 'Scene/Top/Scene'

    @x = 0
    @y = 0
    @width = Config.windowSize.width
    @height = Config.windowSize.height
    @backgroundColor = '#000'
    @select = 'continue'
    @visible = false

    @bg = new Bg()
    @addChild @bg
    @continue = new ContinueLabel(@width / 2 - 100, @height / 2)
    @addChild @continue
    @end = new EndLabel((@width / 2) + 200, @height / 2)
    @addChild @end
    @restart = new RestartLabel((@width / 2) - 400, @height / 2)
    @addChild @restart

  pause: (pause2, cur, scene) ->
    if !pause2
      @setVisible false
      return

    @setVisible true
    @restart.color = @continue.color = @end.color = '#FFF'
    nodes = Collision.eachHitCheck cur, @childNodes
    if Collision.existTypeOfNode nodes, 'end'
      select = 'end'

    else if Collision.existTypeOfNode nodes, 'continue'
      select = 'continue'

    else if Collision.existTypeOfNode nodes, 'restart'
      select = 'restart'

    @touchSelect(scene, select)

  touchSelect: (scene, select) ->
    switch select
      when 'end'
        Se.play 'button2'
        scene.replaceScene new @TopScene

      when 'continue'
        Se.play 'button2'
        scene.pause2 = false

      when 'restart'
        Se.play 'button2'
        scene.replaceScene new scene.Scene

      else

  setVisible: (flg) ->
    @visible = flg
    _(@childNodes).each (node) ->
      node.visible = flg

  getBound: ->
    top   : 0
    left  : 0
    bottom: 0
    right : 0
