Config = require 'Config'

module.exports =
  twistMapCreate: (rotate, map)->
    transpose = (map) ->
        return _.map _(map[0]).keys().reverse(), (x) ->
          return _.map map, (row) ->
            return row[x]
    transposeR = (map) ->
        reverseMap = _(map).reverse()
        return _.map _(reverseMap[0]).keys(), (x) ->
          return _.map reverseMap, (row) ->
            return row[x]
    switch rotate
      when 'L'
        transpose(map)
      else
        transposeR(map)

  twistNode: (rotate, map, node) ->
    currentScene = enchant.Core.instance.currentScene
    size = Config.size
    if !node
      return

    originY = originX = (map.length * size / 2) - (size / 2)
    px = node.x - originX
    py = -(node.y - originY)

    switch rotate
      when 'L'
        r = 90 * Math.PI / 180;
        x = Math.cos(r) * px - Math.sin(r) * py;
        y = Math.cos(r) * py + Math.sin(r) * px;
      else
        r = 270 * Math.PI / 180;
        x = Math.cos(r) * px - Math.sin(r) * py;
        y = Math.cos(r) * py + Math.sin(r) * px;
    x: Math.round(originX + x)
    y: Math.round(originY + -y)
