Config = require 'Config'

module.exports =
  getCollision: (mapData, mapX, mapY, node, direct) ->
    wallBound = @getWallBound(mapX, mapY)
    nodeBound = node.getBound()

    if @isSpace mapData, mapX, mapY
      null
    else if @isNotBound nodeBound, wallBound
      null
    else if direct == 'y'
      @getYCollision nodeBound, wallBound, node
    else
      @getXCollision nodeBound, wallBound, node

  getObjCollision: (node2, node, direct) ->
    if node2 == node
      return null

    nodeBound = node.getBound()
    nodeBound2 = node2.getBound()

    if @isNotBound nodeBound, nodeBound2
      null
    else if direct == 'y'
      @getYCollision nodeBound, nodeBound2, node
    else
      @getXCollision nodeBound, nodeBound2, node

  getYCollision: (nodeBound, wallBound, node) ->
    if @isNodeBottomTouch nodeBound, wallBound
      wallBound.y - (node.rect.y + node.rect.h)
    else
      wallBound.yh - node.rect.y

  getXCollision: (nodeBound, wallBound, node) ->
    if @isNodeRightTouch nodeBound, wallBound
      wallBound.x - (node.rect.x + node.rect.w)
    else
      wallBound.xw - node.rect.x

  isNotBound: (nodeBound, nodeBound2) ->
    nodeBound.xw <= nodeBound2.x or nodeBound.yh <= nodeBound2.y or nodeBound.x >= nodeBound2.xw or nodeBound.y >= nodeBound2.yh

  isNodeBottomTouch: (nodeBound, wallBound) ->
      nodeBound.yh > wallBound.y and nodeBound.y < wallBound.y

  isNodeRightTouch: (nodeBound, wallBound) ->
      nodeBound.xw > wallBound.x and nodeBound.x < wallBound.x

  getWallBound: (mapX, mapY) ->
    size = Config.size
    x: mapX * size
    y: mapY * size
    xw: mapX * size + size
    yh: mapY * size + size

  getMapPos: (pos) ->
    Math.floor (pos / Config.size)

  getPosByDirect: (direct, node) ->
    if direct == 'x' then node.x else node.y

  getCollisions: (mapData, node, direct, mapX, mapY) ->
    _.chain _.range(-1, 2)
    .map (result, x) =>
      _.map _.range(-1, 2), (result, y) =>
        tmpX = mapX + x
        tmpY = mapY + y
        @getCollision mapData, tmpX, tmpY, node, direct
    .flatten()
    .value()

  isSpace: (mapData, mapX, mapY) ->
    data = mapData[mapY] && mapData[mapY][mapX]
    data != '#'

  selectCollision: (collisions, pos) ->
    _.reduce collisions, (result, collision) ->
      if Math.abs pos - collision > Math.abs pos - result
        result
      else
        collision
    , null
