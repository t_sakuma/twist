Config = require 'Config'
Obj = require '../Obj'

module.exports =
  createWalls: (mapDatas, bgAsset) ->
    _.chain mapDatas
    .map (rowMap, y) =>
      _.map rowMap, (data, x) =>
        @createWall data, x, y, bgAsset
    .value()

  createWall: (data, x, y, bgAsset) ->
    core = enchant.Core.instance
    wall = new Obj(x, y)
    wall.type = data
    wall.frame = if data == '#' then 0 else 1
    wall.image = core.assets[bgAsset]
    wall
