Config = require 'Config'

module.exports =
  getMapPos: (node) ->
    rect = node.getBound()
    x: Math.floor (rect.left / Config.size)
    y: Math.floor (rect.top / Config.size)
