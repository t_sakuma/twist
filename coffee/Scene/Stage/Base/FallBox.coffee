Config = require 'Config'
Obj = require './Obj'

module.exports = enchant.Class.create Obj,
  initialize: (x, y) ->
    Obj.call @, x, y
    core = enchant.Core.instance

    @image = core.assets['assets/object.png']
    @frame = 1
    @type = 'FallBox'
    @rect =
      x: 0
      y: 0
      w: 128
      h: 128

  onenterframe: ->
    if @wait
      return

    @motion()

  fall: (gravity) ->
    @y += gravity

  motion: () ->
    @count++
    if (@count > 100000)
      @count = 0

  setLanding: (flg) ->
