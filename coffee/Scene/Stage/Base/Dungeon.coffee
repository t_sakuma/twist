WallGenerator = require './Service/WallGenerator'
StageCollision = require './Service/StageCollision.coffee'
FollowCalc = require './Service/FollowCalc'
TwistCalc = require './Service/TwistCalc'
Se  = require 'Core/Base/Se'

module.exports = enchant.Class.create enchant.Group,
  initialize: (mapAsset, map) ->
    enchant.Group.call @

    @map = map
    @mapAsset = mapAsset

    if @map.length != @map[0].length
      alert '縦:' + @map.length + ' 横:@map[0].length'

    @size = 128
    @walls = @generateWalls @map
    @removeChildren()
    @addChildren(@walls)
    @rotateTime = 15
    @rotating = false

  getNearNodes: (node) ->
    mapPos = StageCollision.getMapPos node
    _.chain(_.range(-1, 2))
    .map (result, x) =>
      _.map _.range(-1, 2), (result, y) =>
        if @walls[mapPos.y + y] && @walls[mapPos.y + y][mapPos.x + x]
          @walls[mapPos.y + y][mapPos.x + x]
        else
          null
    .flatten(true)
    .filter (node) -> node
    .value()

  onenterframe: ->
    currentScene = enchant.Core.instance.currentScene
    pause2 = currentScene.pause2
    death = currentScene.death
    if pause2 or death
      return

    @followPlayer()

  twist: (rotate) ->
    if @rotating
      return
    @rotating = true
    switch rotate
      when 'L'
        Se.play 'twist', 'wav'
        @tl.rotateBy -90, @rotateTime
          .then => 
            @mapTwist rotate
      when 'R'
        Se.play 'twist', 'wav'
        @tl.rotateBy 90, @rotateTime
          .then =>
            @mapTwist rotate 

  mapTwist: (rotate) ->
    @map = TwistCalc.twistMapCreate rotate, @map
    @walls = @generateWalls @map
    @removeChildren()
    @addChildren(@walls)
    @parentNode.followPlayer()
    @rotation = 0
    @rotating = false

  followPlayer: ->
    currentScene = enchant.Core.instance.currentScene
    if !currentScene.player
      return

    origin = FollowCalc.followCalc currentScene.player
    @originX = origin.x
    @originY = origin.y

  generateWalls: (map) ->
    @walls = WallGenerator.createWalls(map, @mapAsset)

  removeChildren: ->
    _.each @childNodes, (node) =>
      @removeChild node

  addChildren:(walls) ->
    _.chain(walls).flatten(true).each (wall) =>
      @addChild wall

  collisionsCheck: (node, direct) ->
    mapX = CollisionCalc.getMapPos node.x
    mapY = CollisionCalc.getMapPos node.y
    collisions = CollisionCalc.getCollisions @map,  node, direct, mapX, mapY
    pos = CollisionCalc.getPosByDirect direct, node
    CollisionCalc.selectCollision collisions, pos
