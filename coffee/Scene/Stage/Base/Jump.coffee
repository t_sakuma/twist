Config = require 'Config'
Obj = require './Obj'

module.exports = enchant.Class.create Obj,
  initialize: (x, y, power = 90) ->
    Obj.call @, x, y
    core = enchant.Core.instance

    @image = core.assets['assets/object.png']
    @frame = 5
    @type = 'Jump'
    @power = power
    @rect =
      x: 16
      y: 16
      w: 96
      h: 96

  onenterframe: ->
    if @wait
      return

    @motion()

  motion: () ->
    @count++
