Config = require 'Config'
LabelObj = require 'Core/Base/LabelObj'

module.exports = enchant.Class.create LabelObj,
  initialize: (text) ->
    LabelObj.call @
    @text = text
    @type = 'startLabel'
    @color = '#FFF'
    @x = Config.windowSize.width / 2 - 400
    @y = Config.windowSize.height / 2 - 50
    @width = 800
    @height = 100
    @font = '100px \'Arial\''
    @textAlign = 'center'
    @originX = 400
    @originY = 50
    @scaleX = 3
    @scaleY = 3

  onenterframe: ->
    @scaleX -= 0.1
    @scaleY -= 0.1
    if @scaleX <= 0
      @remove()
