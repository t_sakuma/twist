Config = require 'Config'
Pointer = require './Pointer'

module.exports = enchant.Class.create Pointer,
  initialize: (x, y, x2 = null, y2 = null, x3 = null, y3 = null) ->
    Pointer.call @, x, y
    core = enchant.Core.instance

    @image = core.assets['assets/boss.png']
    @frame = 0
    @rect =
      x: 0
      y: 0
      w: 128
      h: 128
    @time = 180
    @setProc () =>
      @type = 'none'
    if x2 == null
      if x > 10
        @scaleX = -1
      @tl.moveTo(128 * 10, 128 * 20, @time)
    else if  x3 == null
      if x2 > 10
        @scaleX = -1
      @tl
        .moveTo(x2 * 128, y2 * 128, @time)
        .moveTo(128 * 10, 128 * 10, @time / 2)
    else
      if x3 > 10
        @scaleX = -1
      @tl
        .moveTo(x2 * 128, y2 * 128, @time)
        .moveTo(x3 * 128, y3 * 128, @time / 2)
        .moveTo(128 * 10, 128 * 20, @time / 2)
    

