Scene  = require 'Core/Base/Scene'
Config = require 'Config'
TopLineBg = require './Hud/TopLineBg'
StageNameLabel = require './Hud/StageNameLabel'
MenuLabel = require './Hud/MenuLabel'
StartLabel = require './StartLabel'
Pause = require './Pause/Pause'
CurCtrl = require 'Core/Base/CurCtrl'
Collision = require 'Core/Base/Collision'
StageCollision = require './Service/StageCollision'
Se  = require 'Core/Base/Se'

module.exports = enchant.Class.create Scene,
  initialize: (bgm, stageName, nextScene) ->
    @debug2 = enchant.Core.instance.debug2
    if @debug2
      Scene.call @, 'assets/mami.mp3'
    else
      Scene.call @, bgm

    @createHeader stageName

    @NextScene = nextScene
    @points = []
    @objs = []
    @wait = false

    if @debug2
      @cur.visible = false

    @tl.then () ->
      @wait = @player.ready = true
      @addChild new StartLabel 3
    .delay (30)
    .then () ->
      @addChild new StartLabel 2
    .delay (30)
    .then () ->
      @addChild new StartLabel 1
    .delay (30)
    .then () ->
      @addChild new StartLabel 'START!'
    .delay (30)
    .then () ->
      @wait = false

    @on 'enterframe', () ->
      if @wait
        return

      nodes = Collision.eachHitCheck @cur, @hud2.childNodes
      if Collision.existTypeOfNode nodes, 'menu'
        @pause2 = true

      @PauseGroup.pause(@pause2, @cur, @)

      if @pause2
        return

      if @debug2
        core = enchant.Core.instance
        if @player.image != core.assets['assets/mami.png']
          @player.image = core.assets['assets/mami.png']
        @player.setPos {x: @cur.x * 4, y: @cur.y * 4}
        @playerRunColGoal @player, @objs

      if !@debug2 and @player and @dungeon and @objs
        @playerRun @player, @dungeon, @objs
        @playerFall @player, @dungeon, @objs

      if !@debug2 and @cur and @points and @player
        localCur = @cur.createLocalCur Config.windowSize, @player
        hitPoints = Collision.getTypeOfNode Collision.eachHitCheck(localCur, @points, true), 'point'
        if hitPoints.length > 0
          _.chain(hitPoints)
          .filter (point) -> !point.active
          .each (point) -> point.activeProc()

  playerRun: (player, dungeon, objs) ->
    playerX = player.x
    @playerRunColDun player, dungeon, playerX
    @playerRunColBoss player, @points

  playerRunColDun: (player, dungeon, playerX) ->
    colNodes = Collision.eachHitCheck player, dungeon.getNearNodes(player)
    nodes = Collision.getTypeOfNode colNodes, '#'
    if nodes.length > 0
      @playerXFix player, nodes[0], playerX

  playerRunColBoss: (player, objs) ->
    colNodes = Collision.eachHitCheck player, objs
    if Collision.existTypeOfNode colNodes, 'point'
      @deathProc player

  playerRunColGoal: (player, objs) ->
    colNodes = Collision.eachHitCheck player, objs
    nodes = Collision.getTypeOfNode colNodes, 'Goal'
    if nodes.length > 0
      @goalProc player, nodes[0]

  playerXFix: (player, node, playerX) ->
      if player.x - playerX > 0
        player.x = node.getBound().left - (player.rect.x + player.rect.w)
      else if player.x - playerX < 0
        player.x = node.getBound().right - player.rect.x

  playerFall: (player, dungeon, objs) ->
    playerY = player.y
    @nodeFallColDun player, dungeon, playerY
    @playerFallColBoss player, @points

  nodeFallColDun: (node, dungeon, nodeY) ->
    colNodes = Collision.eachHitCheck node, dungeon.getNearNodes(node)
    nodes = Collision.getTypeOfNode colNodes, '#'
    if nodes.length > 0
      @nodeYFix node, nodes[0], nodeY

  playerFallColBoss: (player, objs) ->
    colNodes = Collision.eachHitCheck player, objs
    if Collision.existTypeOfNode colNodes, 'point'
      @deathProc player

  nodeYFix: (node1, node2,  nodeY) ->
    if node1.y - nodeY > 0
      node1.setLanding true
      node1.y = node2.getBound().top - (node1.rect.y + node1.rect.h)
    else if node1.y - nodeY < 0
      node1.y = node2.getBound().bottom - node1.rect.y

  deathProc: (player) ->
    @wait = true
    @bgm.volume = 0
    player.death()
    @dg.wait = true
    @tl
      .clear()
      .then ->
        Se.play 'deathBgm'
      .delay(100)
      .then ->
        @replaceScene new @Scene()

  goalProc: (player, goal) ->
    @wait = true
    @bgm.volume = 0
    player.wait = true
    @dg.wait = true
    @tl
      .clear()
      .then ->
        Se.play 'deathBgm'
      .delay(100)
      .then ->
        @replaceScene new goal.nextScene

  createHeader: (stageName) ->
    @topLineBg = new TopLineBg()
    @hud2.addChild @topLineBg

    @stageNameLabel = new StageNameLabel(stageName)
    @hud2.addChild @stageNameLabel

    @menuLabel = new MenuLabel()
    @hud2.addChild @menuLabel

    @pause2 = false
    @PauseGroup = new Pause()
    @hud2.addChild @PauseGroup

  addPointer: (point) ->
    @points.push point
    @dg.addChild point

  addObj: (obj) ->
    @objs.push obj
    @dg.addChild obj
