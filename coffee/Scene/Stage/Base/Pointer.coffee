Config = require 'Config'
Obj = require './Obj'
Se  = require 'Core/Base/Se'

module.exports = enchant.Class.create Obj,
  initialize: (x, y) ->
    Obj.call @, x, y
    core = enchant.Core.instance

    @type = 'point'
    @active = false
    @prc = () ->

  activeProc: () ->
    @prc()
    Se.play 'button2'
    @active = true
    @visible = false

  setProc: (f) ->
    @prc = f
