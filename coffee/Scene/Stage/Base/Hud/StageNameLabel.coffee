Config = require 'Config'
LabelObj = require 'Core/Base/LabelObj'

module.exports = enchant.Class.create LabelObj,
  initialize: (text) ->
    LabelObj.call @
    @text = text
    @color = '#FFF'
    @width = Config.windowSize.width
    @x = 0
    @y = 10
    @font = '30px \'Arial\''
    @textAlign = 'center'
    @count = 6000
