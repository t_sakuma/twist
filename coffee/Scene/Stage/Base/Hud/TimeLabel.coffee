Config = require 'Config'
LabelObj = require 'Core/Base/LabelObj'

module.exports = enchant.Class.create LabelObj,
  initialize: () ->
    LabelObj.call @
    @text = ''
    @prefText = 'Time  '
    @color = '#FFF'
    @x = Config.windowSize.width - 150
    @y = 10
    @font = '30px \'Arial\''
    @count = 3000

  runTime: ->
    @text = @prefText + Math.floor(@count / 30)
    @count--

    if @count < 0
      @count = 0

  getTime: ->
    @count
