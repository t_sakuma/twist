Config = require 'Config'
LabelObj = require 'Core/Base/LabelObj'

module.exports = enchant.Class.create LabelObj,
  initialize: () ->
    LabelObj.call @
    core = enchant.Core.instance

    @x = 0
    @y = 0
    @width = Config.windowSize.width
    @height = 50
    @backgroundColor = '#000'
    @opacity = 0.5
