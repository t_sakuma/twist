Config = require 'Config'
LabelObj = require 'Core/Base/LabelObj'

module.exports = enchant.Class.create LabelObj,
  initialize: () ->
    LabelObj.call @
    @text = 'Menu'
    @type = 'menu'
    @color = '#FF6'
    @x = Config.windowSize.width - 300
    @y = 10
    @font = '30px \'Arial\''
