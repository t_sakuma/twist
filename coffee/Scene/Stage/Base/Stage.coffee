Scene  = require 'Core/Base/Scene'
Config = require 'Config'
TopLineBg = require './Hud/TopLineBg'
StageNameLabel = require './Hud/StageNameLabel'
TimeLabel = require './Hud/TimeLabel'
MenuLabel = require './Hud/MenuLabel'
StartLabel = require './StartLabel'
Pause = require './Pause/Pause'
CurCtrl = require 'Core/Base/CurCtrl'
Collision = require 'Core/Base/Collision'
StageCollision = require './Service/StageCollision'
Se  = require 'Core/Base/Se'

module.exports = enchant.Class.create Scene,
  initialize: (bgm, stageName, nextScene) ->
    @debug2 = enchant.Core.instance.debug2
    if @debug2
      Scene.call @, 'assets/mami.mp3'
    else
      Scene.call @, bgm

    @createHeader stageName

    @NextScene = nextScene
    @points = []
    @objs = []
    @wait = false

    if @debug2
      @cur.visible = false

    @tl.then () ->
      @wait = @player.ready = true
      @addChild new StartLabel 3
    .delay (30)
    .then () ->
      @addChild new StartLabel 2
    .delay (30)
    .then () ->
      @addChild new StartLabel 1
    .delay (30)
    .then () ->
      @addChild new StartLabel 'START!'
    .delay (30)
    .then () ->
      @wait = @player.ready = false

    @on 'enterframe', () ->
      if @wait
        return

      nodes = Collision.eachHitCheck @cur, @hud2.childNodes
      if Collision.existTypeOfNode nodes, 'menu'
        @pause2 = true

      @PauseGroup.pause(@pause2, @cur, @)

      if @pause2
        return

      @timeLabel.runTime()

      if @debug2
        core = enchant.Core.instance
        if @player.image != core.assets['assets/mami.png']
          @player.image = core.assets['assets/mami.png']
        @player.setPos {x: @cur.x * 4, y: @cur.y * 4}
        @playerRunColGoal @player, @objs

      if !@debug2 and @player and @timeLabel.getTime() == 0
        @deathProc @player

      if !@debug2 and @player and @dungeon and @objs
        @playerRun @player, @dungeon, @objs
        @playerFall @player, @dungeon, @objs
        _(Collision.getTypeOfNode @objs, 'FallBox').each (obj) =>
          @objFall obj, @dungeon, @objs
        _(Collision.getTypeOfNode @objs, 'Thorn').each (obj) =>
          @objFall obj, @dungeon, @objs

      if !@debug2 and @cur and @points and @player
        localCur = @cur.createLocalCur Config.windowSize, @player
        hitPoints = Collision.getTypeOfNode Collision.eachHitCheck(localCur, @points, true), 'point'
        if hitPoints.length > 0
          _.chain(hitPoints)
          .filter (point) -> !point.active
          .each (point) -> point.activeProc()

  playerRun: (player, dungeon, objs) ->
    playerX = player.x
    player.runAccel player.xAccel
    @playerRunColDun player, dungeon, playerX
    @playerRunColThorn player, objs
    @playerRunColJump player, objs
    @playerRunColGoal player, objs
    @playerRunColObj player, objs, playerX

  playerRunColDun: (player, dungeon, playerX) ->
    colNodes = Collision.eachHitCheck player, dungeon.getNearNodes(player)
    nodes = Collision.getTypeOfNode colNodes, '#'
    if nodes.length > 0
      @playerXFix player, nodes[0], playerX

  playerRunColThorn: (player, objs) ->
    colNodes = Collision.eachHitCheck player, objs
    if Collision.existTypeOfNode colNodes, 'Thorn'
      @deathProc player

  playerRunColJump: (player, objs) ->
    colNodes = Collision.eachHitCheck player, objs
    nodes = Collision.getTypeOfNode colNodes, 'Jump'
    if nodes.length > 0
      Se.play 'jump'
      player.yAccel = nodes[0].power

  playerRunColGoal: (player, objs) ->
    colNodes = Collision.eachHitCheck player, objs
    nodes = Collision.getTypeOfNode colNodes, 'Goal'
    if nodes.length > 0
      @goalProc player, nodes[0]

  playerRunColObj: (player, objs, playerX) ->
    realObj = _.filter objs, (obj) -> obj.type != 'none'
    colNodes = Collision.eachHitCheck player, realObj
    if colNodes.length > 0
      @playerXFix player, colNodes[0], playerX

  playerXFix: (player, node, playerX) ->
      if player.x - playerX > 0
        player.x = node.getBound().left - (player.rect.x + player.rect.w)
      else if player.x - playerX < 0
        player.x = node.getBound().right - player.rect.x

  playerFall: (player, dungeon, objs) ->
    playerY = player.y
    player.fall player.yAccel, player.gravity, player.jampResistivity
    player.setLanding false
    @nodeFallColDun player, dungeon, playerY
    @playerFallColThorn player, objs
    @playerFallColJump player, objs
    @playerFallColGoal player, objs
    @nodeFallColObj player, objs, playerY

  nodeFallColDun: (node, dungeon, nodeY) ->
    colNodes = Collision.eachHitCheck node, dungeon.getNearNodes(node)
    nodes = Collision.getTypeOfNode colNodes, '#'
    if nodes.length > 0
      @nodeYFix node, nodes[0], nodeY

  playerFallColThorn: (player, objs) ->
    colNodes = Collision.eachHitCheck player, objs
    if Collision.existTypeOfNode colNodes, 'Thorn'
      @deathProc player

  playerFallColJump: (player, objs) ->
    colNodes = Collision.eachHitCheck player, objs
    nodes = Collision.getTypeOfNode colNodes, 'Jump'
    if nodes.length > 0
      Se.play 'jump'
      player.yAccel = nodes[0].power

  playerFallColGoal: (player, objs) ->
    colNodes = Collision.eachHitCheck player, objs
    nodes = Collision.getTypeOfNode colNodes, 'Goal'
    if nodes.length > 0
      @goalProc player, nodes[0]

  nodeFallColObj: (node, objs, nodeY) ->
    realObj = _.filter objs, (obj) -> obj.type != 'none'
    colNodes = Collision.eachHitCheck node, realObj
    if colNodes.length > 0
      @nodeYFix node, colNodes[0],  nodeY

  nodeYFix: (node1, node2,  nodeY) ->
    if node1.y - nodeY > 0
      node1.setLanding true
      node1.y = node2.getBound().top - (node1.rect.y + node1.rect.h)
    else if node1.y - nodeY < 0
      node1.y = node2.getBound().bottom - node1.rect.y

  objFall: (obj, dungeon, objs) ->
    objY = obj.y
    obj.fall obj.gravity
    @nodeFallColDun obj, dungeon, objY
    @nodeFallColObj obj, objs, objY

  deathProc: (player) ->
    @wait = true
    @bgm.volume = 0
    player.death()
    @dg.wait = true
    @tl
      .then ->
        Se.play 'deathBgm'
      .delay(100)
      .then ->
        @replaceScene new @Scene()

  goalProc: (player, goal) ->
    @wait = true
    @bgm.volume = 0
    player.wait = true
    @dg.wait = true
    @tl
      .then ->
        Se.play 'deathBgm'
      .delay(100)
      .then ->
        @replaceScene new goal.nextScene

  createHeader: (stageName) ->
    @topLineBg = new TopLineBg()
    @hud2.addChild @topLineBg

    @stageNameLabel = new StageNameLabel(stageName)
    @hud2.addChild @stageNameLabel

    @timeLabel = new TimeLabel()
    @hud2.addChild @timeLabel

    @menuLabel = new MenuLabel()
    @hud2.addChild @menuLabel

    @pause2 = false
    @PauseGroup = new Pause()
    @hud2.addChild @PauseGroup

  addPointer: (point) ->
    @points.push point
    @dg.addChild point

  addObj: (obj) ->
    @objs.push obj
    @dg.addChild obj
