Config = require 'Config'
Pointer = require './Pointer'

module.exports = enchant.Class.create Pointer,
  initialize: (x, y) ->
    Pointer.call @, x, y
    core = enchant.Core.instance

    @image = core.assets['assets/object.png']
    @frame = 3
    @rect =
      x: 0
      y: 0
      w: 128
      h: 128
    @setProc () =>
      currentScene = enchant.Core.instance.currentScene
      dungeon = currentScene.dungeon
      player = currentScene.player
      objs = currentScene.objs
      points = currentScene.points
      if !dungeon or !player or !objs or !points
        return
      @tl
      .then () ->
        currentScene.wait = true
        player.wait = true
        _(objs).each (obj) =>
          obj.wait = true
          obj.visible = false

        _(points).each (point) =>
          point.wait = true
          point.visible = false
      .delay dungeon.rotateTime
      .then () ->
        player.setPos @nodeTwist(player, dungeon.map)
        player.wait = false
        _(objs).each (obj) =>
          obj.setPos @nodeTwist(obj, dungeon.map)
          obj.wait = false
          obj.visible = true

        _(points).each (point) =>
          point.setPos @nodeTwist(point, dungeon.map)
          point.wait = false
          point.visible = !point.active
        currentScene.wait = false
      dungeon.twist('R')

  onenterframe: ->
    if @wait
      return

    @motion()

  motion: () ->
    @count++
    @tl.rotateBy 3, 0
    if (@count > 100000)
      @count = 0

  nodeTwist: (node, map) ->
    size = Config.size
    if !node
      return

    originY = originX = (map.length * size / 2) - (size / 2)
    px = node.x - originX
    py = -(node.y - originY)

    r = 270 * Math.PI / 180;
    x = Math.cos(r) * px - Math.sin(r) * py
    y = Math.cos(r) * py + Math.sin(r) * px
    x: Math.round((originX + x) / 128) * 128
    y: Math.round((originY + -y) / 128) * 128
