Config = require 'Config'
Se  = require 'Core/Base/Se'
Obj = require './Obj'
TwistCalc = require './Service/TwistCalc'
CollisionCalc = require './Service/CollisionCalc'

module.exports = enchant.Class.create Obj,
  initialize: (x, y, nextScene) ->
    Obj.call @, x, y
    core = enchant.Core.instance

    @image = core.assets['assets/object.png']
    @frame = 6
    @type = 'Goal'
    @rect =
      x: 48
      y: 64
      w: 48
      h: 64
    @rotateTime = 15
    @rotating = false
    @nextScene = nextScene

  onenterframe: ->
    if @wait
      return

    @motion()

  motion: () ->
    @count++
    if (@count > 100000)
      @count = 0
