Config = require 'Config'
Obj = require './Obj'

module.exports = enchant.Class.create Obj,
  initialize: (x, y, moveDist) ->
    Obj.call @, x, y
    core = enchant.Core.instance

    @image = core.assets['assets/object.png']
    @frame = 4
    @moveDist = @x + moveDist * 128
    @type = 'FlBox'
    @rect =
      x: 0
      y: 0
      w: 128
      h: 128

  onenterframe: ->
    if @wait
      return

    @move @moveDist
    @motion()

  move: (moveDist) ->
    if @x < moveDist
      @x += @xAccel
    else
      @type = 'none'
      @remove()

  motion: () ->
    @count++
    if (@count > 100000)
      @count = 0
