Config = require 'Config'
CollisionCalc = require './Service/CollisionCalc'
Obj = require './Obj'
Se  = require 'Core/Base/Se'

module.exports = enchant.Class.create Obj,
  initialize: (x, y) ->
    Obj.call @, x, y
    core = enchant.Core.instance

    @image = core.assets['assets/char.png']
    @frame = 0
    @gravity = 25
    @jumpPower = 60
    @jampResistivity = 4
    @haveBox = 5
    @rotateTime = 15
    @wait = false
    @rect =
      x: 43
      y: 9
      w: 42
      h: 119
    @Box = require 'Scene/Stage/Base/Box'

  onenterframe: ->
    currentScene = enchant.Core.instance.currentScene
    pause2 = currentScene.pause2
    if pause2
      return

    @count++
    if (@count > 100000)
      @count = 0

    if @wait
      return

    @frame = if @ready 
      @nomalMotion(@count)
    else
      @jumpMotion(@landing) or @runMotion(@count, @landing)

  jumpAccel: (jumpPower) ->
    Se.play 'jump'
    @yAccel = jumpPower

  runAccel: (xAccel) ->
    @x += xAccel

  fall: (yAccel, gravity, jampResistivity) ->
    @y += gravity - yAccel
    @yAccel -= jampResistivity
    if @yAccel < 3
      @yAccel = 0

  setLanding: (flg) ->
    @landing = flg

  nomalMotion: (count, ready) ->
    frame = Math.floor(count / 7)
    frame % 1

  runMotion: (count, landing) ->
    if landing
      Math.floor(count / 3) % 4 + 5

  jumpMotion: (landing) ->
    if  !landing
      10
    else
      0

  death: ->
    @wait = true
    @tl
      .then ->
        Se.play 'deathVoice'
        @frame = 15
      .delay(1)
      .moveBy(0, -200, 22)
      .delay(10)
      .then ->
        @frame = 15
      .delay(10)
      .then ->
        @frame = 15
      .delay(10)
      .then ->
        @frame = 15
      .delay(10)
      .moveBy(0, 900, 22)
