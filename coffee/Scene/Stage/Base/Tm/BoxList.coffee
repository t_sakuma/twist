Config = require 'Config'
CollisionCalc = require './Service/CollisionCalc'
TwistCalc = require './Service/TwistCalc'

module.exports = enchant.Class.create enchant.Group,
  initialize: () ->
    enchant.Group.call @
    @rotateTime = 15
    @rotating = false

  onLButtonDown: ->
    currentScene = enchant.Core.instance.currentScene
    pause2 = currentScene.pause2
    death = currentScene.death
    if pause2 or death
      return

    @twist('L')

  onRButtonDown: ->
    currentScene = enchant.Core.instance.currentScene
    pause2 = currentScene.pause2
    death = currentScene.death
    if pause2 or death
      return

    @twist('R')

  collisionsCheck: (node, direct) ->
    checkPoint = @getCheckPoint node, direct
    @getHitPos @childNodes, node, checkPoint, direct

  collisionsThornCheck: (node, direct) ->
    @getThornHitPos @childNodes, node, direct

  getCheckPoint: (node, direct) ->
    if direct == 'x' then node.x else node.y

  getHitPos: (children, node, checkPoint, direct) ->
    _.reduce children, (result, child) =>
      col = CollisionCalc.getObjCollision child, node, direct
      if Math.abs checkPoint - col > Math.abs checkPoint - result
        result
      else
        col
    , null

  getThornHitPos: (children, node, direct) ->
    _.reduce children, (result, child) =>
      if child.type == 'Thorn'

        result || CollisionCalc.getObjCollision child, node, direct
      else
        result
    , null

  twist: (rotate) ->
    if @rotating
      return
    @rotating = true
    _.each @childNodes, (node) =>
      node.visible = false
    @tl.delay @rotateTime
      .then =>
        @boxTwist rotate

  boxTwist: (rotate) ->
    @twistNodes rotate
    _.each @childNodes, (node) =>
      node.visible = true
    @rotating = false

  twistNodes: (rotate) ->
    dungeon = enchant.Core.instance.currentScene.dungeon
    if dungeon
      _.each @childNodes, (node) =>
          pos = TwistCalc.twistNode rotate, dungeon.map, node
          node.setPos pos
