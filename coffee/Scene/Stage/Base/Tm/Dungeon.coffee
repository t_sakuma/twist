WallGenerator = require './Service/WallGenerator'
CollisionCalc = require './Service/CollisionCalc'
FollowCalc = require './Service/FollowCalc'
TwistCalc = require './Service/TwistCalc'

module.exports = enchant.Class.create enchant.Group,
  initialize: (map) ->
    enchant.Group.call @

    @map = map

    if @map.length != @map[0].length
      alert '縦:' + @map.length + ' 横:@map[0].length'

    @size = 128
    @generateWalls @map
    @rotateTime = 15
    @rotating = false

  onLButtonDown: ->
    currentScene = enchant.Core.instance.currentScene
    pause2 = currentScene.pause2
    death = currentScene.death
    if pause2 or death
      return

    @twist('L')

  onRButtonDown: ->
    currentScene = enchant.Core.instance.currentScene
    pause2 = currentScene.pause2
    death = currentScene.death
    if pause2 or death
      return

    @twist('R')

  onenterframe: ->
    currentScene = enchant.Core.instance.currentScene
    pause2 = currentScene.pause2
    death = currentScene.death
    if pause2 or death
      return

    @followPlayer()

  twist: (rotate) ->
    if @rotating
      return
    @rotating = true
    switch rotate
      when 'L'
        @tl.rotateBy -90, @rotateTime
          .then => @mapTwist rotate
      when 'R'
        @tl.rotateBy 90, @rotateTime
          .then => @mapTwist rotate 

  mapTwist: (rotate) ->
    player = enchant.Core.instance.currentScene.player
    pos = TwistCalc.twistNode rotate, @map, player
    player.setPos pos
    @map = TwistCalc.twistMapCreate rotate, @map
    @generateWalls @map
    @parentNode.followPlayer()
    @rotation = 0
    @rotating = false

  followPlayer: ->
    currentScene = enchant.Core.instance.currentScene
    if !currentScene.player
      return

    origin = FollowCalc.followCalc currentScene.player
    @originX = origin.x
    @originY = origin.y

  generateWalls: (map) ->
    @removeChildren()
    walls = WallGenerator.createWalls(map, 'assets/dg.png')
    @addChildren(walls)

  removeChildren: ->
    _.each @childNodes, (node) =>
      @removeChild node

  addChildren:(walls) ->
    _.each walls, (wall) =>
      @addChild wall

  collisionsCheck: (node, direct) ->
    mapX = CollisionCalc.getMapPos node.x
    mapY = CollisionCalc.getMapPos node.y
    collisions = CollisionCalc.getCollisions @map,  node, direct, mapX, mapY
    pos = CollisionCalc.getPosByDirect direct, node
    CollisionCalc.selectCollision collisions, pos
