Config = require 'Config'
Obj = require './Obj'

module.exports = enchant.Class.create Obj,
  initialize: (x, y) ->
    Obj.call @, x, y
    core = enchant.Core.instance

    @image = core.assets['assets/object.png']
    @frame = 0
    @gravity = 25
    @type = 'Box'
    @rect =
      x: 0
      y: 0
      w: 128
      h: 128

  onenterframe: ->
    currentScene = enchant.Core.instance.currentScene
    pause2 = currentScene.pause2
    death = currentScene.death
    if pause2 or death
      return

    if @visible
      @fall()
      @motion()

  fall: ->
    @y += @gravity
    max = @collisionsCheck @, 'y'
    @landing = false
    if max != null
      @y = max
      @y += 1
      max = @collisionsCheck @, 'y'
      @y -= 1
      if max != null
        @landing = true

    @pressHit()

  pressHit: () ->
    player = enchant.Core.instance.currentScene.player
    pressHit = player.collisionsPressCheck @
    if pressHit != null
      player.death()

  motion: () ->
    @count++
    if (@count > 100000)
      @count = 0

  collisionsCheck: (node, direct)->
    currentScene = enchant.Core.instance.currentScene
    max = currentScene.dungeon.collisionsCheck node, direct
    max2 = currentScene.boxs.collisionsCheck node, direct
    max || max2
