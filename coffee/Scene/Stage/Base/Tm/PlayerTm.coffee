Config = require 'Config'
CollisionCalc = require './Service/CollisionCalc'
Obj = require './Obj'
Se  = require 'Core/Base/Se'

module.exports = enchant.Class.create Obj,
  initialize: (x, y) ->
    Obj.call @, x, y
    core = enchant.Core.instance

    @image = core.assets['assets/char.png']
    @frame = 0
    @gravity = 25
    @jumpPower = 60
    @jampResistivity = 4
    @xAccel = 10
    @yAccel = 0
    @haveBox = 5
    @rotateTime = 15
    @rotating = false
    @rect =
      x: 43
      y: 9
      w: 42
      h: 119
    @Box = require 'Scene/Stage/Base/Box'

  onAButtonDown: ->
    currentScene = enchant.Core.instance.currentScene
    pause2 = currentScene.pause2
    death = currentScene.death
    if pause2 or death or @count == 0
      return

    if @haveBox <= 0
      return
    bound = @getBound()
    mapX = CollisionCalc.getMapPos bound.x
    mapY = CollisionCalc.getMapPos bound.y
    currentScene = enchant.Core.instance.currentScene

    @x = mapX * Config.size
    @y = mapY * Config.size
    if @scaleX == 1
      box = new @Box(mapX + 1, mapY)
    else
      box = new @Box(mapX - 1, mapY)
    max = @collisionsBoxCheck box, 'x'
    if max == null
      Se.play 'addbox'
      @haveBox--
      currentScene.boxs.addChild box

  onLButtonDown: ->
    currentScene = enchant.Core.instance.currentScene
    pause2 = currentScene.pause2
    death = currentScene.death
    if pause2 or death
      return

    @twist()

  onRButtonDown: ->
    currentScene = enchant.Core.instance.currentScene
    pause2 = currentScene.pause2
    death = currentScene.death
    if pause2 or death
      return

    @twist()

  onenterframe: ->
    currentScene = enchant.Core.instance.currentScene
    pause2 = currentScene.pause2
    death = currentScene.death
    if pause2 or death
      return

    if @rotating
      return

    @runAccel @xAccel
    @xCol

    @fall()
    @frame = @jumpMotion(@landing) or @runMotion(@count, @landing) or @motion(@count)
    @count++
    if (@count > 100000)
      @count = 0

  jumpAccel: (jumpPower) ->
    Se.play 'jump'
    @yAccel = jumpPower

  fall: ->
    @y += @gravity - @yAccel
    hit = @collisionsThornCheck 'x'
    if hit != null
      @death()

    max = @collisionsCheck 'y'
    @landing = false
    if max != null
      @y = max
      @y += 1
      max = @collisionsCheck 'y'
      @y -= 1
      if max != null
        @landing = true
      else
        @yAccel /= 2

    @yAccel -= @jampResistivity
    if @yAccel < 3
      @yAccel = 0

  runAccel: (xAccel) ->
    @x += xAccel

  xCol: () ->
    hit = @collisionsThornCheck 'x'
    if hit != null
      @death()

    max = @collisionsCheck 'x'
    if max != null
      @x = max

  motion: (count) ->
    frame = Math.floor(count / 7)
    @frame = frame % 4

  runMotion: (count, landing) ->
    if landing
      count % 5 + 5

  jumpMotion: (landing) ->
    if  !landing
      11
    else
      0

  twist: () ->
    if @rotating
      return
    @rotating = true
    @tl
      .then ->
        Se.play 'twist', 'wav'
      .delay @rotateTime
      .then =>
        @rotating = false

  collisionsCheck: (direct)->
    currentScene = enchant.Core.instance.currentScene
    max = currentScene.dungeon.collisionsCheck @, direct
    max2 = currentScene.boxs.collisionsCheck @, direct
    max || max2

  collisionsThornCheck: (direct)->
    currentScene = enchant.Core.instance.currentScene
    currentScene.boxs.collisionsThornCheck @, direct

  collisionsBoxCheck: (box, direct)->
    currentScene = enchant.Core.instance.currentScene
    max = currentScene.dungeon.collisionsCheck box, direct
    max2 = currentScene.boxs.collisionsCheck box, direct
    max || max2

  collisionsPressCheck: (node)->
    if @landing == true
      CollisionCalc.getObjCollision @,  node, 'y'
    else
      null

  death: ->
    currentScene = enchant.Core.instance.currentScene
    currentScene.death = true
    currentScene.bgm.volume = 0
    @tl
      .then ->
        Se.play 'deatsh'
        Se.play 'deathbgm'
        @frame = 15
      .delay(1)
      .moveBy(0, -200, 22)
      .delay(10)
      .then ->
        @frame = 16
      .delay(10)
      .then ->
        @frame = 17
      .delay(10)
      .then ->
        @frame = 18
      .delay(10)
      .moveBy(0, 900, 22)
      .delay(10)
      .then ->
        currentScene.replaceScene new currentScene.Scene()
