Scene  = require 'Core/Base/Scene'
Config = require 'Config'
TopLineBg = require './Hud/TopLineBg'
StageNameLabel = require './Hud/StageNameLabel'
HaveBoxLabel = require './Hud/HaveBoxLabel'
TimeLabel = require './Hud/TimeLabel'
MenuLabel = require './Hud/MenuLabel'
Pause = require './Pause/Pause'
CurCtrl = require 'Core/Base/CurCtrl'
Collision = require 'Core/Base/Collision'

module.exports = enchant.Class.create Scene,
  initialize: (bgm, stageName, nextScene) ->
    Scene.call @, bgm
    @addOperationTarget @

    @NextScene = nextScene

    @topLineBg = new TopLineBg()
    @hud2.addChild @topLineBg

    @stageNameLabel = new StageNameLabel(stageName)
    @hud2.addChild @stageNameLabel

    @haveBoxLabel = new HaveBoxLabel()
    @hud2.addChild @haveBoxLabel

    @timeLabel = new TimeLabel()
    @hud2.addChild @timeLabel

    @menuLabel = new MenuLabel()
    @hud2.addChild @menuLabel

    @pause2 = false
    @death = false
    @PauseGroup = new Pause()
    @hud2.addChild @PauseGroup

    @on 'enterframe', () ->
      nodes = Collision.eachHitCheck @cur, @hud2.childNodes
      if Collision.existTypeOfNode nodes, 'menu'
        @pause2 = true

      @PauseGroup.pause(@pause2, @cur, @)

      # if @player and @dungeon and @key.leap and hand = @key.getHand()
      #   curPos = CurCtrl.handToPos(hand, Config.windowSize)
      #   @player.setCurPos CurCtrl.worldToLocal(curPos, Config.windowSize, @player), @cur.getBound()

  onPauseButtonDown: ->
    if @pause2
      @pause2 = false
    else
      @pause2 = true
