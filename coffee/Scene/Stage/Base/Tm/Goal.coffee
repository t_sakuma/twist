Config = require 'Config'
Se  = require 'Core/Base/Se'
Obj = require './Obj'
TwistCalc = require './Service/TwistCalc'
CollisionCalc = require './Service/CollisionCalc'

module.exports = enchant.Class.create Obj,
  initialize: (x, y) ->
    Obj.call @, x, y
    core = enchant.Core.instance

    @image = core.assets['assets/dg.png']
    @frame = 12
    @type = 'Goal'
    @rect =
      x: 48
      y: 64
      w: 48
      h: 64
    @rotateTime = 15
    @rotating = false

  onenterframe: ->
    if @visible
      @goal()
      @motion()

  onLButtonDown: ->
    currentScene = enchant.Core.instance.currentScene
    pause2 = currentScene.pause2
    death = currentScene.death
    if pause2 or death
      return

    @twist('L')

  onRButtonDown: ->
    currentScene = enchant.Core.instance.currentScene
    pause2 = currentScene.pause2
    death = currentScene.death
    if pause2 or death
      return

    @twist('R')

  twist: (rotate) ->
    dungeon = enchant.Core.instance.currentScene.dungeon
    if @rotating
      return
    @rotating = true
    @visible = false
    @tl.delay @rotateTime
      .then =>
        if dungeon
          pos = TwistCalc.twistNode rotate, dungeon.map, @ 
          @setPos pos
        @visible = true
        @rotating = false

  goal: () ->
    currentScene = enchant.Core.instance.currentScene
    if !currentScene.death and @collisionCheck()
      currentScene.death = true
      @tl
        .then ->
          currentScene.bgm.volume = 0
          Se.play 'clear'
        .delay 120
        .then ->
          currentScene.replaceScene new currentScene.NextScene()

  motion: () ->
    @count++
    if (@count > 100000)
      @count = 0

  collisionCheck: () ->
    currentScene = enchant.Core.instance.currentScene
    playerBound = currentScene.player.getBound()
    nodeBound = @getBound()

    if CollisionCalc.isNotBound nodeBound, playerBound
      false
    else
      true

