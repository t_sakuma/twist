module.exports = enchant.Class.create enchant.Sprite,
  initialize: (x, y) ->
    enchant.Sprite.call @
    core = enchant.Core.instance

    @x = x * 128
    @y = y * 128
    @width = 128
    @height = 128
    @frame = 0
    @gravity = 25
    @landing = false
    @xAccel = 5
    @yAccel = 0
    @count = 0
    @wait = false
    @type = ''
    @rect =
      x: 0
      y: 0
      w: 128
      h: 128

  setPos: (pos) ->
    @x = pos.x
    @y = pos.y


  getBound: ->
    left : @x + @rect.x
    top : @y + @rect.y
    right: @x + @rect.x + @rect.w
    bottom: @y + @rect.y + @rect.h
