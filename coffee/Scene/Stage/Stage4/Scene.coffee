Config  = require 'Config'
Scene   = require 'Scene/Stage/Base/BossStage'
NextScene  = require 'Scene/StaffRoll/Scene'
BossPoint = require 'Scene/Stage/Base/BossPoint'
Player  = require 'Scene/Stage/Base/Player'
Goal    = require 'Scene/Stage/Base/Goal'
Bg      = require './Bg'
ClearLabel      = require './ClearLabel'
Dungeon = require './Dungeon'
Se  = require 'Core/Base/Se'

module.exports = enchant.Class.create Scene,
  initialize: () ->
    Scene.call @, 'assets/stage4.mp3', 'ボス戦', require 'Scene/Stage/Stage4/Scene'
    @Scene = require 'Scene/Stage/Stage4/Scene'

    @bg.addChild new Bg()

    @dungeon = new Dungeon()
    @dg.addChild @dungeon

    @addObj new Goal(1, 1, NextScene)

    @player = new Player(10, 20)
    @dg.addChild @player

    @tl
    .delay 180
    .then () ->
      @addPointer new BossPoint(_.random(1, 20), _.random(16, 19))
    .delay 60
    .then () ->
      @addPointer new BossPoint(_.random(1, 20), _.random(16, 19))
    .delay 60
    .then () ->
      @addPointer new BossPoint(_.random(1, 20), _.random(16, 19))
    .delay 60
    .then () ->
      @addPointer new BossPoint(_.random(1, 20), _.random(16, 19))
    .delay 60
    .then () ->
      @addPointer new BossPoint(_.random(1, 20), _.random(16, 19))
    .delay 60
    .then () ->
      @addPointer new BossPoint(_.random(1, 20), _.random(16, 19))
    .delay 60
    .then () ->
      @addPointer new BossPoint(_.random(1, 20), _.random(16, 19))
    .delay 60
    .then () ->
      @addPointer new BossPoint(_.random(1, 20), _.random(16, 19))
    .delay 60
    .then () ->
      @addPointer new BossPoint(_.random(1, 20), _.random(16, 19))
    .delay 60
    .then () ->
      @addPointer new BossPoint(_.random(1, 20), _.random(16, 19))
    .delay 60
    .then () ->
      @addPointer new BossPoint(_.random(1, 20), _.random(16, 19))
    .delay 60
    .then () ->
      @addPointer new BossPoint(_.random(1, 20), _.random(16, 19))
    .delay 60
    .then () ->
      @addPointer new BossPoint(_.random(1, 20), _.random(1, 19), _.random(1, 20), _.random(1, 21), _.random(1, 20), _.random(16, 21))
    .delay 60
    .then () ->
      @addPointer new BossPoint(_.random(1, 20), _.random(1, 19), _.random(1, 20), _.random(1, 21), _.random(1, 20), _.random(16, 21))
    .delay 60
    .then () ->
      @addPointer new BossPoint(_.random(1, 20), _.random(1, 19), _.random(1, 20), _.random(1, 21), _.random(1, 20), _.random(16, 21))
    .delay 60
    .then () ->
      @addPointer new BossPoint(_.random(1, 20), _.random(1, 19), _.random(1, 20), _.random(1, 21), _.random(1, 20), _.random(16, 21))
    .delay 60
    .then () ->
      @addPointer new BossPoint(_.random(1, 20), _.random(1, 19), _.random(1, 20), _.random(1, 21), _.random(1, 20), _.random(16, 21))
    .delay 60
    .then () ->
      @addPointer new BossPoint(_.random(1, 20), _.random(1, 19), _.random(1, 20), _.random(1, 21), _.random(1, 20), _.random(16, 21))
    .delay 60
    .then () ->
      @addPointer new BossPoint(_.random(1, 20), _.random(1, 19), _.random(1, 20), _.random(1, 21), _.random(1, 20), _.random(16, 21))
    .delay 60
    .then () ->
      @addPointer new BossPoint(_.random(1, 20), _.random(1, 19), _.random(1, 20), _.random(1, 21), _.random(1, 20), _.random(16, 21))
    .delay 60
    .then () ->
      @addPointer new BossPoint(_.random(1, 20), _.random(1, 19), _.random(1, 20), _.random(1, 21), _.random(1, 20), _.random(16, 21))
    .delay 60
    .then () ->
      @addPointer new BossPoint(_.random(1, 20), _.random(1, 19), _.random(1, 20), _.random(1, 21), _.random(1, 20), _.random(16, 21))
    .delay 400
    .then () ->
      @hud2.addChild new ClearLabel('Congratulation!!')
      @bgm.volume = 0
      Se.play 'deathBgm'
    .delay 150
    .then () ->
      @clear = true

    @on 'enterframe', () ->
      if (!@wait && @clear)
        @wait = true
        @replaceScene new NextScene()
