Config = require 'Config'
LabelObj = require 'Core/Base/LabelObj'

module.exports = enchant.Class.create LabelObj,
  initialize: (text) ->
    LabelObj.call @
    @text = text
    @type = 'Label'
    @color = '#FFF'
    @x = Config.windowSize.width / 2 - 400
    @y = Config.windowSize.height / 2 - 200
    @width = 800
    @height = 50
    @font = '50px \'Arial\''
    @textAlign = 'center'
    @originX = 400
    @originY = 25
    @scaleX = 3
    @scaleY = 3
