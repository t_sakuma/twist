Config  = require 'Config'
Scene   = require 'Scene/Stage/Base/Stage'
NextScene  = require 'Scene/Stage/Stage3/Scene'
Goal    = require 'Scene/Stage/Base/Goal'
Thorn   = require 'Scene/Stage/Base/Thorn'
FallThorn   = require 'Scene/Stage/Base/FallThorn'
Jump   = require 'Scene/Stage/Base/Jump'
BoxPoint = require 'Scene/Stage/Base/BoxPoint'
FlBoxPoint = require 'Scene/Stage/Base/FlBoxPoint'
FallBoxPoint = require 'Scene/Stage/Base/FallBoxPoint'
TwistPoint = require 'Scene/Stage/Base/TwistPoint'
Player  = require 'Scene/Stage/Base/Player'
Bg      = require './Bg'
Dungeon = require './Dungeon'

module.exports = enchant.Class.create Scene,
  initialize: () ->
    Scene.call @, 'assets/stage2.mp3', 'ノコギリパニック', require 'Scene/Stage/Stage2/Scene'
    @Scene = require 'Scene/Stage/Stage2/Scene'

    @bg.addChild new Bg()

    @dungeon = new Dungeon()
    @dg.addChild @dungeon

    @addObj new Goal(20, 1, NextScene)

    @player = new Player(1, 19)
    @dg.addChild @player

    #@addPointer new BoxPoint(4, 20)
    @addObj new FallThorn(3,12)
    @addObj new FallThorn(4,12)
    @addObj new FallThorn(6,4)
    @addObj new FallThorn(6,5)
    @addObj new FallThorn(6,6)
    @addObj new FallThorn(6,7)
    @addObj new Thorn(20, 14)
    @addObj new Thorn(20, 15)

    @addObj new Thorn(10, 11)
    @addObj new Thorn(11, 11)
    @addObj new Thorn(12, 11)
    @addObj new Thorn(13, 11)
    @addObj new Thorn(14, 11)
    @addObj new Thorn(15, 11)
    @addPointer new TwistPoint(6, 19)
    @addPointer new TwistPoint(18, 12)
    @addPointer new TwistPoint(1, 1)
    @addPointer new TwistPoint(1, 6)
    @addPointer new TwistPoint(18, 9)
    @addPointer new FlBoxPoint(9, 10, 5)
