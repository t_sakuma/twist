Config  = require 'Config'
Scene   = require 'Scene/Stage/Base/Stage'
Gaol    = require 'Scene/Stage/Base/Goal'
BoxList = require 'Scene/Stage/Base/BoxList'
Box     = require 'Scene/Stage/Base/Box'
Thorn   = require 'Scene/Stage/Base/Thorn'
Player  = require 'Scene/Stage/Base/Player'
Bg      = require './Bg'
Dungeon = require './Dungeon'

module.exports = enchant.Class.create Scene,
  initialize: () ->
    Scene.call @, 'assets/stage1.mp3', 'チュートリアルコース', require 'Scene/Stage/Stage1/Scene'
    @Scene = require 'Scene/Stage/Stage1/Scene'

    @bg.addChild new Bg()

    @dungeon = new Dungeon()
    @addOperationTarget @dungeon
    @dg.addChild @dungeon

    @goal = new Gaol(1,8)
    @dg.addChild @goal
    @addOperationTarget @goal

    @boxs = new BoxList()
    @dg.addChild @boxs
    @addOperationTarget @boxs
    @boxs.addChild new Box(1, 3)
    @boxs.addChild new Box(2, 1)
    @boxs.addChild new Box(2, 2)
    @boxs.addChild new Thorn(7, 7)

    @player = new Player(2, 5)
    @dg.addChild @player
    @addOperationTarget @player
