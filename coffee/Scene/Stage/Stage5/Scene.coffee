Config  = require 'Config'
Scene   = require 'Scene/Stage/Base/Stage'
Goal    = require 'Scene/Stage/Base/Goal'
Thorn   = require 'Scene/Stage/Base/Thorn'
Jump   = require 'Scene/Stage/Base/Jump'
BoxPoint = require 'Scene/Stage/Base/BoxPoint'
FlBoxPoint = require 'Scene/Stage/Base/FlBoxPoint'
FallBoxPoint = require 'Scene/Stage/Base/FallBoxPoint'
TwistPoint = require 'Scene/Stage/Base/TwistPoint'
Player  = require 'Scene/Stage/Base/Player'
Bg      = require './Bg'
Dungeon = require './Dungeon'

module.exports = enchant.Class.create Scene,
  initialize: () ->
    Scene.call @, 'assets/stage4.mp3', 'キャッスルマウンテン', require 'Scene/Stage/Stage5/Scene'
    @Scene = require 'Scene/Stage/Stage5/Scene'

    @bg.addChild new Bg()

    @dungeon = new Dungeon()
    @dg.addChild @dungeon

    NextScene  = require 'Scene/Top/Scene'
    @addObj new Goal(1, 12, NextScene)

    @player = new Player(1, 18)
    @dg.addChild @player

    @addPointer new BoxPoint(2, 6)
    @addPointer new BoxPoint(3, 6)
    @addPointer new BoxPoint(4, 6)
    @addPointer new BoxPoint(5, 6)
    @addPointer new BoxPoint(6, 5)
    @addPointer new BoxPoint(7, 6)
    @addPointer new BoxPoint(8, 6)
    @addPointer new BoxPoint(7, 15)
    @addPointer new BoxPoint(9, 12)
    @addPointer new BoxPoint(12, 2)
    @addPointer new BoxPoint(13, 2)
    @addPointer new BoxPoint(12, 13)
    @addPointer new BoxPoint(17, 2)
    @addPointer new BoxPoint(18, 2)
    @addPointer new BoxPoint(17, 8)
    @addPointer new BoxPoint(18, 8)
    @addPointer new TwistPoint(0, 3)
    @addPointer new TwistPoint(0, 10)
    @addPointer new TwistPoint(9, 5)
    @addPointer new TwistPoint(11, 16)
    @addPointer new TwistPoint(11, 2)
    @addPointer new TwistPoint(15, 17)
    @addPointer new TwistPoint(17, 15)
    @addPointer new TwistPoint(17, 17)
    @addPointer new TwistPoint(18, 1)
    @addPointer new TwistPoint(20, 2)
    @addPointer new FallBoxPoint(3, 5)
    @addPointer new FallBoxPoint(4, 4)
    @addPointer new FallBoxPoint(5, 5)
    @addPointer new FallBoxPoint(6, 6)
    @addPointer new FallBoxPoint(7, 5)
    @addPointer new FallBoxPoint(8, 4)
    @addPointer new FallBoxPoint(7, 16)
    @addPointer new FallBoxPoint(7, 17)
    @addPointer new FlBoxPoint(10, 17, 10)
    @addPointer new FlBoxPoint(8, 10, 5)
    @addPointer new FlBoxPoint(11, 8, 9)
    @addPointer new FlBoxPoint(12, 19, 8)
    @addPointer new FlBoxPoint(19, 18, 6)
    @addPointer new FlBoxPoint(18, 13, 7)
    @addObj new Jump(2, 3)
    @addObj new Jump(13, 11)
    @addObj new Jump(14, 10)
    @addObj new Jump(16, 1)
    @addObj new Jump(19, 9)
    @addObj new Thorn(3, 3)
    @addObj new Thorn(4, 3)
    @addObj new Thorn(5, 3)
    @addObj new Thorn(6, 3)
    @addObj new Thorn(7, 3)
    @addObj new Thorn(8, 3)
    @addObj new Thorn(12, 1)
    @addObj new Thorn(13, 1)
    @addObj new Thorn(14, 12)
    @addObj new Thorn(15, 12)
    @addObj new Thorn(16, 12)
    @addObj new Thorn(17, 12)
    @addObj new Thorn(18, 12)
    @addObj new Thorn(19, 12)
    @addObj new Thorn(20, 12)
    @addObj new Thorn(12, 20)
    @addObj new Thorn(13, 20)
    @addObj new Thorn(14, 20)
    @addObj new Thorn(15, 20)
    @addObj new Thorn(16, 20)
    @addObj new Thorn(17, 20)
    @addObj new Thorn(18, 20)
    @addObj new Thorn(19, 20)
    @addObj new Thorn(20, 20)
    @addObj new Thorn(20, 13)
    @addObj new Thorn(20, 14)
    @addObj new Thorn(20, 15)
    @addObj new Thorn(20, 16)
    @addObj new Thorn(20, 17)
    @addObj new Thorn(20, 18)
    @addObj new Thorn(20, 19)
