Config = require 'Config'

module.exports = enchant.Class.create enchant.Label,
  initialize: () ->
    enchant.Label.call @
    @text = '魔法少女製作委員会'
    @_element = document.createElement 'div'
    @_element.id = 'logo'
    @width = Config.windowSize.width
    @height = null
    @x = 0
    @y = 250
    @font = '100px \'Arial\''
    @textAlign = 'center'
