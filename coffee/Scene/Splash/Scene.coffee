Scene  = require 'Core/Base/Scene'

module.exports = enchant.Class.create Scene,
  initialize: () ->
    Scene.call @, ''
    Bg = require './Bg'
    @hud.addChild new Bg()
    Logo = require './Logo'
    @hud.addChild new Logo()
    @NextScene = require 'Scene/Top/Scene'
    @tl
      .delay 60
      .then ->
        @replaceScene new @NextScene()
