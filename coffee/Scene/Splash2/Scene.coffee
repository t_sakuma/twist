Scene  = require 'Core/Base/Scene'

module.exports = enchant.Class.create Scene,
  initialize: () ->
    Scene.call @, ''
    Bg = require './Bg'
    @bg.addChild new Bg()
    Logo = require './Logo'
    @bg.addChild new Logo()
    @NextScene = require 'Scene/Top/Scene'
    @tl
      .delay 60
      .then ->
        @replaceScene new @NextScene()
