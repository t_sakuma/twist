Config = require 'Config'

module.exports = enchant.Class.create enchant.Label,
  initialize: () ->
    enchant.Label.call @
    @text = '魔法少女製作委員会'
    @width = Config.windowSize.width
    @height = 100
    @originX = Config.windowSize.width / 2
    @originY = @height / 2
    @x = 0
    @y = (Config.windowSize.height / 2) - @originY
    @font = '100px \'Arial\''
    @textAlign = 'center'
    @color = '#FFF'
    @opacity = 0

    @tl
      .fadeTo 1, 30
