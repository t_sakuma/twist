Config = require 'Config'

module.exports = enchant.Class.create enchant.Label,
  initialize: () ->
    enchant.Label.call @
    @text = ''
    @width = Config.windowSize.width
    @height = Config.windowSize.height
    @backgroundColor = '#000'
    @x = 0
    @y = 0
