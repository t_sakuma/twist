module.exports =
  windowSize:
    width: 1440
    height: 850
  assets: [
    'assets/map0.png'
    'assets/cur.png'
    'assets/title.png'
    'assets/curTrajectory.png'
    'assets/char.png'
    'assets/boss.png'
    'assets/mami.png'
    'assets/dg.png'
    'assets/item.png'
    'assets/map1.png'
    'assets/map2.png'
    'assets/map3.png'
    'assets/map4.png'
    'assets/bg.png'
    'assets/object.png'
    'assets/stage1.mp3'
    'assets/stage2.mp3'
    'assets/stage3.mp3'
    'assets/stage4.mp3'
    'assets/op.mp3'
    'assets/ed.mp3'
    'assets/addbox.mp3'
    'assets/button1.mp3'
    'assets/button2.mp3'
    'assets/clear.mp3'
    'assets/damage.mp3'
    'assets/deathVoice.mp3'
    'assets/deathBgm.mp3'
    'assets/jump.mp3'
    'assets/run.mp3'
    'assets/twist.wav'
    'assets/mou.wav'
    'assets/mami.mp3'
  ]
  fps: 30
  size: 128
  nodeType:
    core     : 'core'
    scene    : 'scene'
    key      : 'scene/key'
    bg       : 'scene/bg'
    dungeon  : 'scene/dungeon'
    hud      : 'scene/hud'
