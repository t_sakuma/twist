Config = require 'Config'
Scene = require 'Scene/Splash2/Scene'

enchant()

window.onload = ->
  core = new Core(Config.windowSize.width, Config.windowSize.height)
  core.fps = Config.fps
  core.preload Config.assets

  # DEBUG
  arg = {}
  pair = location.search.substring(1).split('&')
  _.each pair, (p, k) ->
      kv = p.split '='
      arg[kv[0]] = kv[1]

  core.onload = ->
    if arg.debug == '1'
      DebugScene = require 'Scene/Debug/Scene'
      core.pushScene new DebugScene()
    else
      core.pushScene new Scene()

  core.start()
