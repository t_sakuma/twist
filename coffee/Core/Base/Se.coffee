module.exports =
  play: (seName, ext = 'mp3') ->
    if seName
      core = enchant.Core.instance
      core.assets["assets/#{seName}.#{ext}"].clone().play()
