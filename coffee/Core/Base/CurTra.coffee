module.exports = enchant.Class.create enchant.Sprite,
  initialize: (x, y) ->
    enchant.Sprite.call @
    @size = 64
    @x = x + _.random -15, 25
    @y = y
    @width = @size
    @height = @size
    @opacity = 0.5
    @frame = 0
    @image = enchant.Core.instance.assets['assets/curTrajectory.png']
    @count = 0

  onenterframe: ->
    if @count > 15
      @remove()
    @y += 5
    @opacity = @opacity - 0.02
    if @opacity < 0
      @opacity = 0
    @count++
