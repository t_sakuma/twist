Config   = require 'Config'
Dg   = require './DungeonGround'
GamePad = require 'Core/Pad/GamePad'
Cur = require './Cur'
CurCtrl = require './CurCtrl'
Collision = require './Collision'

module.exports = enchant.Class.create enchant.Scene,
  initialize: (bgm = '') ->
    enchant.Scene.call @

    @bg = new enchant.Group
    @addChild @bg

    @dg = new Dg
    @addChild @dg

    # BGM設定
    core = enchant.Core.instance
    @bgm = if bgm != '' then core.assets[bgm].clone() else null
    @on 'enterframe', ->
      if @bgm && @bgm.volume != 0
        @bgm.play()

    # DIVタグ描画用
    @hud2 = new enchant.Group
    @addChild @hud2

    @hud = new enchant.Group
    @hud._element = document.createElement 'div'
    @addChild @hud

    # コントローラ設定
    @key = new GamePad()
    @controller = GamePad
    @addChild @key

    # カーソル
    @cur = new Cur
    @addChild @cur

    @debug = new enchant.Group
    @addChild @debug
    @dbLabel = new enchant.Label
    @dbLabel.x = 10
    @dbLabel.y = 50
    @dbLabel.font = '20px \'Arial\''
    @fpstotal = 0
    @fpscount = 0
    @debug.addChild @dbLabel

  onenterframe: (e) ->
    if GamePad.input
      if @key.leap and hand = @key.getHand()
        @cur.setPos CurCtrl.handToPos hand, Config.windowSize

        point =
          x: hand.indexFinger.tipPosition[0]
          y: hand.indexFinger.tipPosition[1]

    # DEBUG
    @fpstotal += e.elapsed
    @fpscount++
    if  @fpstotal >= 1000
        @dbLabel.text = """
          #{(1000 / @fpstotal * @fpscount).toFixed(2)} FPS
        """
        @fpstotal = 0
        @fpscount = 0

  replaceScene: (scene) ->
    if @bgm
      @bgm.volume = 0
      @bgm.stop()

    if @bg.childNodes.length > 0
      @bg.remove()

    if @dg.childNodes.length > 0
      _.each @dg.childNodes.length, (node) ->
        _.each node.childNodes.length, (node) ->
          _.each node.childNodes.length, (node) ->
            node.remove()
          node.remove()
        node.remove()

    if @player != undefined
      @player.remove()
    _.each @hud.childNodes.length, (node) ->
      node.remove()

    _.each @hud2.childNodes.length, (node) ->
      node.remove()

    if @cur
      @cur.remove()

    if @debug
      _.each @debug.childNodes.length, (node) ->
        node.remove()

    _.each @childNodes.length, (node) ->
      node.remove()

    if @key != null
      @key.remove()
    enchant.Core.instance.replaceScene scene
