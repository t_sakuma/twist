module.exports =
  eachHitCheck: (node1, nodes) ->
    _(nodes).filter (node) =>
      @hitCheck node1, node

  hitCheck: (node1, node2) ->
    rect1 = node1.getBound()
    rect2 = node2.getBound()
    if rect1.right <= rect2.left or
    rect1.bottom <= rect2.top or
    rect1.left >= rect2.right or
    rect1.top >= rect2.bottom
      false
    else if node1 == node2
      false
    else
      true

  createBound: (node) ->
    top   : node.y
    left  : node.x
    bottom: node.y + node.height
    right : node.x + node.width

  existTypeOfNode: (nodes, type) ->
    @getTypeOfNode(nodes, type).length > 0

  getTypeOfNode: (nodes, type) ->
    _(nodes).filter (node) ->
      node.type == type
