module.exports = enchant.Class.create enchant.Sprite,
  initialize: (x, y, width, height, asset) ->
    enchant.Sprite.call @, x, y

    @width = width || null
    @height = height || null
    @image = core.assets[asset]
