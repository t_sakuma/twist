Collision = require './Collision'

module.exports =
  handToPos: (hand, windowSize) ->
    x: (windowSize.width / 2) + hand.indexFinger.tipPosition[0] * 10
    y: windowSize.height - (hand.indexFinger.tipPosition[1] - 100) * 10

  worldToLocal: (pos, windowSize, node) ->
    rect = node.getBound()
    x: pos.x - (windowSize.width / 2 - rect.right)
    y: pos.y - (windowSize.height / 2 - rect.bottom)
