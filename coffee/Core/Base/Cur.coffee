CurTra = require './CurTra'

module.exports = enchant.Class.create enchant.Sprite,
  initialize: () ->
    enchant.Sprite.call @
    @size = 64
    @x = 0
    @y = 0
    @width = @size
    @height = @size
    @originX = @size / 2
    @originy = @size / 2
    @opacity = 0.5
    @frame = 0
    @image = enchant.Core.instance.assets['assets/cur.png']

  onenterframe: ->
    core = enchant.Core.instance
    if core.frame % 7 == 0
      @trajectory @x, @y, core.frame

  setPos: (pos) ->
    @x = pos.x
    @y = pos.y

  getBound: ->
    top   : @y
    left  : @x
    bottom: @y + @height
    right : @x + @width

  setVisible: (flg) ->
    @visible = flg

  createLocalCur: (windowSize, node) ->
    getBound: () =>
      rect = node.getBound()
      x = @x - (windowSize.width / 2 - rect.right)
      y = @y - (windowSize.height / 2 - rect.top)
      top: y
      left: x
      bottom: y + @height
      right : x + @width

  trajectory: (x, y) ->
    @parentNode.addChild new CurTra(x, y)

