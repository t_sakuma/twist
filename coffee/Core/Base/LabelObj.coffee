Config = require 'Config'

module.exports = enchant.Class.create enchant.Label,
  initialize: () ->
    enchant.Label.call @

  getBound: ->
    top   : @y
    left  : @x
    bottom: @y + @height
    right : @x + @width
