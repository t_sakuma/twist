GamePad = enchant.Class.create enchant.Entity,
  initialize: () ->
    enchant.Entity.call @

    @leap = new Leap.Controller()
    @leap.connect()

    @nodeList = []

    if !GamePad.oldInput
      GamePad.oldInput = {
        AxisX: 0
        AxisY: 0
        A    : false
        B    : false
        L    : false
        R    : false
        Pause: false
      }

    GamePad.buttons =
      'Pause': 11
      'L'    : 4
      'A'    : 3
      'B'    : 2
      'R'    : 5

  getHand: ->
    frame = @leap.frame()
    if (!frame.valid || frame.hands.length == 0)
      return
    else
      frame.hands[0]

  onenterframe: ->
    GamePad.input = {
      AxisX: 0
      AxisY: 0
    }

    frame = @leap.frame()
    if (!frame.valid || frame.hands.length == 0)
      return

    hand = frame.hands[0]

    axisX = (hand.yaw() + 0.1) * 3
    axisX = if axisX > 1 then 1 else axisX
    axisX = if axisX < -1 then -1 else axisX
    GamePad.input.AxisX = axisX
    GamePad.input.AxisY = 0

module.exports = GamePad
