ツイストワールド（仮
====

魔法少女製作委員会による合宿用ゲーム

## 説明
基本的にcoffee scriptで書かれているがjsにも対応している  
画像と音楽はassetsに配置する

## インストール
* nodeをインストール
* npm install webpack -g
* npm install

## コンパイル
    // 通常のコンパイル
    webpack
    
    // 変更を監視して自動コンパイル
    webpack --watch
